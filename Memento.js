// Inspired by https://sourcemaking.com/design_patterns/memento

class Edidor {
  constructor() {
    this._current = undefined;
  }

  get current() {
    return this._current;
  }

  set current(value) {
    this._current = value;
  }

  createState() {
    return new EdidorState(this._current);
  }
  undo(state) {
    this._current = state?._content;
  }
}

class History {
  constructor() {
    this.states = [];
  }
  push(state) {
    this.states.push(state);
  }
  pop() {
    const state = this.states;
    state.pop();
    const lastIndex = state.length - 1;
    return state[lastIndex];
  }
}

class EdidorState {
  constructor(content) {
    this._content = content;
  }
  get content() {
    return this._content;
  }
}

const editor = new Edidor();
const history = new History();

editor.current = "a";
history.push(editor.createState());

editor.current = "b";
history.push(editor.createState());

editor.current = "c";
history.push(editor.createState());

editor.undo(history.pop());
editor.undo(history.pop());

console.log(editor._current);
